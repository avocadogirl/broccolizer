FROM ruby:2.7

# Descargar los repositorios necesarios
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo 'deb https://dl.yarnpkg.com/debian/ stable main' | tee /etc/apt/sources.list.d/yarn.list

# Instalar las dependencias
RUN apt-get update && apt-get install -y nodejs yarn postgresql-client

# Configurar el entorno de trabajo
RUN mkdir /broccolizer
WORKDIR /broccolizer
COPY Gemfile /broccolizer/Gemfile
COPY Gemfile.lock /broccolizer/Gemfile.lock
RUN bundle install
COPY . /broccolizer

# Configurar el entrypoint de Rails
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Levantar el proceso principal
CMD ["rails", "server", "-b", "0.0.0.0"]
